﻿using System;
using System.Collections.Generic;
using System.Text;
using Plugin.Iconize;
using Xamarin.Forms;
using System.Threading.Tasks;
using FormsPlugin.Iconize;
using System.Threading;

namespace TestIconize
{
    class AnimatedIcon : IconImage
    {
        private CancellationTokenSource cts;
        private bool isForwardRotation = true;
        private double defaultRotation = 0.0;

        public BindableProperty SpinProperty =
            BindableProperty.Create("Spin", 
                typeof(String), 
                typeof(AnimatedIcon), 
                "none",
                propertyChanged: OnSpinChanged
            );

        public BindableProperty FlipProperty =
            BindableProperty.Create("Flip",
                typeof(String),
                typeof(AnimatedIcon),
                "0",
                propertyChanged: OnFlipChanged
            );

        public String Flip
        {
            get { return (String)GetValue(FlipProperty); }
            set { SetValue(FlipProperty, value); }
        }

        public String Spin
        {
            get { return (String)GetValue(SpinProperty); }
            set { SetValue(SpinProperty, value); }
        }

        static void OnFlipChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var flipType = newValue as String;
            var im = bindable as AnimatedIcon;

            if (flipType.Contains("v"))
            {
                im.RotationX = 180;
                flipType = flipType.Replace("v", "");
            }
            else
            {
                im.RotationX = 0;
            }

            if (flipType.Contains("h"))
            {
                im.RotationY = 180;
                flipType = flipType.Replace("h", "");
            }
            else
            {
                im.RotationY = 0;
            }

            try
            {
                double rotation = 0;

                if (flipType != "")
                {
                    rotation = double.Parse(flipType);
                }
                
                im.Rotation = rotation;
                im.defaultRotation = rotation;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }        
        }

        static void OnSpinChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var spinType = newValue as String;
            var myImage = bindable as AnimatedIcon;

            if (spinType.Equals("fw"))
            {
                myImage.isForwardRotation = true;
            }
            else if (spinType.Equals("rw")) {
                myImage.isForwardRotation = false;                
            }
            else
            {
                return;
            }
        }
           
        public void EnableAnimation()
        {
            if (cts == null)
            {
                cts = new CancellationTokenSource();
                this.RotateElement(isForwardRotation, defaultRotation, cts.Token);
            }
        }

        public void DisableAnimation()
        {
            if (cts != null)
            {
                cts.Cancel();
                cts = null;
            }
        }

        private async Task RotateElement(bool isForward, double defaultRotation, CancellationToken cancellation)
        {            
            int rotation = (isForward ? 1 : -1) * 360;
            uint duration = 2000;

            while (!cancellation.IsCancellationRequested)
            {
                await this.RotateTo(defaultRotation + rotation, duration, Easing.Linear);
                this.Rotation = defaultRotation;
            }
        }
    }


}
