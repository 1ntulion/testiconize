﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Plugin.Iconize;

using Xamarin.Forms;

namespace TestIconize
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.FontAwesomeModule());
            
            MainPage = new TestIconize.MainPage();		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
