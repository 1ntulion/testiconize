﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Iconize;

namespace TestIconize 
{
	public partial class MainPage : ContentPage
	{
        private bool isAnimated = true;

		public MainPage()
		{
			InitializeComponent();

            myIcon.EnableAnimation();
            mySecondIcon.EnableAnimation();

        }

        private void OnButtonClicked(object sender, System.EventArgs e)
        {
            Button button = (Button)sender;

            if (isAnimated)
            {
                myIcon.DisableAnimation();
                mySecondIcon.DisableAnimation();
                isAnimated = false;               

                button.Text = "Включить анимацию";
            }
            else
            {
                myIcon.EnableAnimation();
                mySecondIcon.EnableAnimation();
                isAnimated = true;

                button.Text = "Выключить анимацию";
            }
            
        }
    }
}
