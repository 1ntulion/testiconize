﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using FormsPlugin.Iconize;

namespace TestIconize
{
    class StackedIcons : Grid
    {
        public BindableProperty IconsProperty =
            BindableProperty.Create("Icons",
                typeof(String),
                typeof(StackedIcons),
                String.Empty,
                propertyChanged: OnIconsChanged
            );   

        static void OnIconsChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var iconProperty = newValue as String;
            var grid = bindable as StackedIcons;

            // Имя иконки всегда первый  параметр
            // размер - "s:"
            // цвет (hex) - "c:"
            // поворот/отражение - "f:" - vh90 или h90 или v или 180
            // Пример: "fa-circle, c:000000, s:50; fa-flag, c:FFFFFF, s:25, f:h45"
            String[] arrayIcon = iconProperty.Split(';');

            foreach (String icon in arrayIcon)
            {
                String[] array = icon.Split(',');
                AnimatedIcon im = new AnimatedIcon();

                im.Icon = array[0].Trim();

                foreach (String element in array)
                {                
                    try
                    {
                        String property = element.Trim();

                        if (property.Contains("s:"))
                        {
                            im.IconSize = int.Parse(property.Substring(2));
                        }
                        else if (property.Contains("c:"))
                        {
                            im.IconColor = Color.FromHex(property.Substring(2));
                        }
                        else if (property.Contains("f:"))
                        {
                            im.Flip = property.Substring(2);                            
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }
                }

                grid.Children.Add(im);
            }
        }

        public String Icons
        {
            get { return (String)GetValue(IconsProperty); }
            set { SetValue(IconsProperty, value); }
        }

        public StackedIcons()
        {
            this.ColumnDefinitions.Add( new ColumnDefinition() );
            this.RowDefinitions.Add( new RowDefinition() );
        }
    }
}
